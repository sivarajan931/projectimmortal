﻿Imports System.IO.Ports
Public Class Form3
    Dim x As String
    Dim y As Boolean
    Dim s As String
    Dim SAPI As Object = CreateObject("sapi.spvoice")
    Private Sub Form3_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Panel1.BackColor = My.Settings.mycolor
        Panel1.ForeColor = Color.White
        Button1.ForeColor = My.Settings.mycolor
        Button2.ForeColor = My.Settings.mycolor
        Button3.ForeColor = Color.White
        Button4.ForeColor = Color.White
        Button5.ForeColor = Color.White
        Button6.ForeColor = Color.White
        Button7.ForeColor = Color.White
        Button8.ForeColor = Color.White
        Button9.ForeColor = Color.White
        Label3.ForeColor = Color.White
        Label4.ForeColor = Color.White
        Label5.ForeColor = Color.White
        Label7.ForeColor = Color.White
        Label8.ForeColor = Color.White
        Label9.ForeColor = Color.White
        Label10.ForeColor = Color.White
        Label12.ForeColor = Color.White
        Panel2.BackColor = My.Settings.mycolor
        Panel3.BackColor = My.Settings.mycolor
        ComboBox1.BackColor = My.Settings.mycolor
        For Each sp As String In My.Computer.Ports.SerialPortNames
            ComboBox1.Items.Add(sp)
        Next
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If x = "" Then
            MsgBox("No device has been selected.", MsgBoxStyle.Information, "Information")
        Else
            Try
                With SerialPort1
                    .PortName = x
                    .BaudRate = My.Settings.br
                    .Open()
                End With
                y = True
            Catch ex As Exception
                MsgBox("Could not connect to the Serial Port.", MsgBoxStyle.Critical, "Error")
                SAPI.Speak("Could not connect to the Serial Port")
            End Try
        End If
        If y = True Then
            Button1.Enabled = False
            Button2.Enabled = True
            SAPI.Speak("Connected Successfully")
        End If

    End Sub
    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        x = ComboBox1.Text
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        SerialPort1.Close()
        Button1.Enabled = True
        Button2.Enabled = False
        y = False
        SAPI.Speak("Dis connected Successfully")
    End Sub
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Try
            SerialPort1.Write("2")
        Catch ex As Exception
            er()
        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Try
            SerialPort1.Write("6")
        Catch ex As Exception
        End Try
    End Sub
    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Try
            SerialPort1.Write("8")
        Catch ex As Exception
            er()
        End Try
    End Sub
    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Try
            SerialPort1.Write("5")
        Catch ex As Exception
            er()
        End Try
        er()
    End Sub
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Try
            SerialPort1.Write("4")
        Catch ex As Exception
            er()
        End Try
    End Sub
    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Try

            SerialPort1.Write("1")

        Catch ex As Exception
            er()
        End Try
    End Sub
    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        Try
            SerialPort1.Write("0")
        Catch ex As Exception
            er()
        End Try
    End Sub
    Private Sub TrackBar1_Scroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TrackBar1.Scroll
        Try
            SerialPort1.Write(TrackBar1.Value)
        Catch ex As Exception
            er()
        End Try
    End Sub
    Private Sub TrackBar2_Scroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TrackBar2.Scroll
        Try
            SerialPort1.Write(TrackBar2.Value)
        Catch ex As Exception
            er()
        End Try
    End Sub
    Public Sub er()
        MsgBox("Could not send the bytes to the Serial Port.", MsgBoxStyle.Critical, "Error")
        SAPI.Speak("Could not send the bytes to the serial port.")
    End Sub

End Class