﻿Imports System.Speech.Recognition
Imports System.Threading
Imports System.Globalization
Public Class Form4
    Dim recog As New SpeechRecognizer
    Dim gram As Grammar
    Dim x As String
    Dim y As Boolean
    Dim SAPI As Object = CreateObject("sapi.spvoice")
    Public Event SpeechRecognized As EventHandler(Of SpeechRecognizedEventArgs)
    Public Event SpeechRecognitionRejected As EventHandler(Of SpeechRecognitionRejectedEventArgs)
    Dim wordlist As String() = New String() {"front", "forward", "backward", "back", "left", "right", "on", "off", "bright", "dim", "fast", "slow", "stop"}
    Public Sub recfailevent(ByVal sender As System.Object, ByVal e As RecognitionEventArgs)
        My.Computer.Audio.PlaySystemSound(System.Media.SystemSounds.Asterisk)
    End Sub
    Public Sub recevent(ByVal sender As System.Object, ByVal e As RecognitionEventArgs)
        Try
            If (e.Result.Text.IndexOf("front") Or e.Result.Text.IndexOf("forward")) Then
                SerialPort1.Write("2")
            ElseIf (e.Result.Text.IndexOf("back") Or e.Result.Text.IndexOf("forward")) Then
                SerialPort1.Write("8")
            ElseIf (e.Result.Text.IndexOf("left")) Then
                SerialPort1.Write("4")
            ElseIf (e.Result.Text.IndexOf("right")) Then
                SerialPort1.Write("6")
            ElseIf (e.Result.Text.IndexOf("stop")) Then
                SerialPort1.Write("5")
            ElseIf (e.Result.Text.IndexOf("on")) Then
                SerialPort1.Write("1")
            ElseIf (e.Result.Text.IndexOf("off")) Then
                SerialPort1.Write("0")
            ElseIf (e.Result.Text.IndexOf("bright") Or e.Result.Text.IndexOf("fast")) Then
                SerialPort1.Write("225")
            ElseIf (e.Result.Text.IndexOf("dim") Or e.Result.Text.IndexOf("slow")) Then
                SerialPort1.Write("100")
            End If
        Catch ex As Exception
            er()
        End Try
    End Sub
    Private Sub Form4_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Thread.CurrentThread.CurrentCulture = New CultureInfo("en-GB")
        Thread.CurrentThread.CurrentUICulture = New CultureInfo("en-GB")
        Dim words As New Choices(wordlist)
        gram = New Grammar(New GrammarBuilder(words))
        recog.LoadGrammar(gram)
        AddHandler recog.SpeechRecognized, AddressOf Me.recevent
        AddHandler recog.SpeechRecognitionRejected, AddressOf Me.recfailevent
        recog.Enabled = True
        Panel1.BackColor = My.Settings.mycolor
        Panel1.ForeColor = Color.White
        Button1.ForeColor = My.Settings.mycolor
        Button2.ForeColor = My.Settings.mycolor
        ComboBox1.BackColor = My.Settings.mycolor
        For Each sp As String In My.Computer.Ports.SerialPortNames
            ComboBox1.Items.Add(sp)
        Next
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If x = "" Then
            MsgBox("No device has been selected.", MsgBoxStyle.Information, "Information")
        Else
            Try
                With SerialPort1
                    .PortName = x
                    .BaudRate = My.Settings.br
                    .Open()
                End With
                y = True
            Catch ex As Exception
                MsgBox("Could not connect to the Serial Port.", MsgBoxStyle.Critical, "Error")
                SAPI.Speak("Could not connect to the Serial Port")
            End Try
        End If
        If y = True Then
            Button1.Enabled = False
            Button2.Enabled = True
            SAPI.Speak("Connected Successfully")
        End If
    End Sub
    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        x = ComboBox1.Text
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        SerialPort1.Close()
        Button1.Enabled = True
        Button2.Enabled = False
        y = False
        SAPI.Speak("Dis connected Successfully")
    End Sub
    Public Sub er()
        MsgBox("Could not send the bytes to the serialport.", MsgBoxStyle.Critical, "Error")
        SAPI.Speak("Could not send the bytes to the serial port.")
    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub
End Class