﻿Public Class Form6
    Private Sub Form6_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Panel1.BackColor = My.Settings.mycolor
        Panel1.ForeColor = Color.White
        Button1.ForeColor = My.Settings.mycolor
        ComboBox1.BackColor = My.Settings.mycolor
        ColorDialog1.Color = My.Settings.mycolor
        TextBox2.Text = My.Settings.br
        sd()
    End Sub
    Private Sub Button1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If ColorDialog1.ShowDialog() = DialogResult.OK Then
            My.Settings.mycolor = ColorDialog1.Color
            Panel1.BackColor = My.Settings.mycolor
            Button1.ForeColor = My.Settings.mycolor
            ComboBox1.BackColor = My.Settings.mycolor
        End If
    End Sub
    Private Sub ComboBox1_SelectedIndexChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Panel2.Visible = True
        TextBox1.Text = ComboBox1.Text
    End Sub
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Panel2.Visible = False
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If ComboBox1.Text = My.Settings.d1 Then
            My.Settings.d1 = TextBox1.Text
        ElseIf ComboBox1.Text = My.Settings.d2 Then
            My.Settings.d2 = TextBox1.Text
        ElseIf ComboBox1.Text = My.Settings.d3 Then
            My.Settings.d3 = TextBox1.Text
        ElseIf ComboBox1.Text = My.Settings.d4 Then
            My.Settings.d4 = TextBox1.Text
        ElseIf ComboBox1.Text = My.Settings.d5 Then
            My.Settings.d5 = TextBox1.Text
        ElseIf ComboBox1.Text = My.Settings.d6 Then
            My.Settings.d6 = TextBox1.Text
        End If
        sd()
        Panel2.Visible = False
    End Sub
    Public Sub sd()
        ComboBox1.Items.Clear()
        ComboBox1.Text = String.Empty
        ComboBox1.Items.Add(My.Settings.d1)
        ComboBox1.Items.Add(My.Settings.d2)
        ComboBox1.Items.Add(My.Settings.d3)
        ComboBox1.Items.Add(My.Settings.d4)
        ComboBox1.Items.Add(My.Settings.d5)
        ComboBox1.Items.Add(My.Settings.d6)
    End Sub
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        My.Settings.br = TextBox2.Text
    End Sub
End Class