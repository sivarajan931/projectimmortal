﻿Public Class Form5
    Dim SAPI As Object = CreateObject("sapi.spvoice")
    Dim x As String
    Dim y As Boolean
    Private Sub Form5_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Panel1.BackColor = My.Settings.mycolor
        ComboBox1.BackColor = My.Settings.mycolor
        Button1.ForeColor = My.Settings.mycolor
        Button2.ForeColor = My.Settings.mycolor
        Button3.ForeColor = My.Settings.mycolor
        Label3.ForeColor = Color.White
        For Each sp As String In My.Computer.Ports.SerialPortNames
            ComboBox1.Items.Add(sp)
        Next
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If x = "" Then
            MsgBox("No device has been selected.", MsgBoxStyle.Information, "Information")
            SAPI.Speak("No device has been selected")
        Else
            Try
                With SerialPort1
                    .PortName = x
                    .BaudRate = My.Settings.br
                    .Open()
                End With
                y = True
            Catch ex As Exception
                MsgBox("Could not connect to the Serial Port.", MsgBoxStyle.Critical, "Error")
                SAPI.Speak("Could not connect to the Serial Port")
            End Try
        End If
        If y = True Then
            Button1.Enabled = False
            Button2.Enabled = True
            SAPI.Speak("Connected Successfully")
        End If
    End Sub
    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        x = ComboBox1.Text
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        SerialPort1.Close()
        Button1.Enabled = True
        Button2.Enabled = False
        y = False
        SAPI.Speak("Dis connected Successfully")
    End Sub
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Try
            SerialPort1.Write(TextBox1.Text)
        Catch ex As Exception
            MsgBox("Could not send the string to the serialport.", MsgBoxStyle.Critical, "Error")
            SAPI.Speak("Could not send the string to the serialport.")
        End Try
    End Sub
End Class